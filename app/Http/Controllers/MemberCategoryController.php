<?php

namespace App\Http\Controllers;

use App\MemberCategory;
use Illuminate\Http\Request;

class MemberCategoryController extends Controller
{
    public function index(){
        $membercategory = MemberCategory::all();
        return view('pages.member_categories', compact('membercategory'));
    }

    public function destroy($id)
    {
        $membercategory = MemberCategory::findOrFail($id);
        $membercategory->delete();
        return redirect('member_categories');
    
    }

    public function store()
    {

        request()->validate([
            'name' => 'required',
        ]);
        $membercategory = new MemberCategory();
        $membercategory->name = request('name');
        $membercategory->save();

        return redirect('member_categories');
    
    }

    public function update(MemberCategory $membercategory, $id)
    {
        request()->validate([
            'name' => 'required',
        ]);

        $membercategories = $membercategory::findOrFail($id);
        $membercategories->name = request('name');
        $membercategories->update();

        return redirect('member_categories');

    }

    public function edit(MemberCategory $membercategory, $id)
    {
        $membercategory = MemberCategory::findOrFail($id);
        return view('pages.member_categories.editm', compact('membercategory'));
        // return response()->json($member_categories);
    }
    
}