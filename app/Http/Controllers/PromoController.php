<?php

namespace App\Http\Controllers;

use App\promo;
use App\Product;
use Illuminate\Http\Request;

class PromoController extends Controller
{
    public function index(){
        $products = product::all();
        $promos = promo::all();
        return view('pages.promo', compact('products', 'promos'));
    }
    public function destroy($id)
    {
        $promos=promo::findOrFail($id);
        $promos->delete();
        return back();
    
    }

    public function store()
    {

        request()->validate([
            'name' => 'required',
            'product_category_id' => 'required',
            'discount' => 'required',
        ]);
        $promos = new promo();
        $promos->name = request('name');
        $promos->product_category_id = request('product_category_id');
        $promos->discount = request('discount');
        $promos->save();

        return redirect('promos');
    }

    public function update(Promo $promo)
    {
        $promo->update([
        'name' => request('name'),
        'product_category_id' => request('product_category_id'),
        'discount' => request('discount'),
        ]);

        return redirect('promos');

    }

    public function edit(Promo $promo)
    {
        $products = Product::all();
        return view('pages.promos.editprom', compact('promo', 'products'));
        // return response()->json($promos);
    }
}