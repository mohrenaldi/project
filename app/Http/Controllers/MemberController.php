<?php

namespace App\Http\Controllers;

use App\Member;
use App\MemberCategory;
use Illuminate\Http\Request;

class MemberController extends Controller
{
    public function index(){
    $members = Member::all();
    $membercategory = MemberCategory::all();
    return view('pages.member', compact('members', 'membercategory'));
    
    }

    public function destroy($id)
    {
        $member = Member::findOrFail($id);
        $member->delete();
        return back();
    
    }

    public function store(Request $request)
    {

        $request->validate([
            'member_category_id' => 'required',
            'fullname' => 'required',
            'dob' => 'required',
            'address' => 'required',
            'gender' => 'required',
        ]);

        $rmdash = str_replace("-","",$request->dob);
        $rmcolon = str_replace(":","",$rmdash);
        $rmspace = str_replace(" ","",$rmcolon).substr(str_shuffle('0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ'),1,3);
        $barcode = 'BRC'.$rmspace;


        $member = new Member();
        $member->member_category_id = $request->member_category_id;
        $member->fullname = $request->fullname;
        $member->dob = $request->dob;
        $member->address = $request->address;
        $member->gender = $request->gender;
        $member->barcode = $barcode;
        $member->save();

        return redirect('members');
    
    }

    public function update(Member $member)
    {
        request()->validate([
            'member_category_id' => 'required',
            'fullname' => 'required',
            'dob' => 'required',
            'address' => 'required',
            'gender' => 'required',
            'barcode' => 'required',
        ]);

        
        $member->member_category_id = request('member_category_id');
        $member->fullname = request('fullname');
        $member->dob = request('dob');
        $member->address = request('address');
        $member->gender = request('gender');
        $member->barcode = request('barcode');
        $member->update();

        return redirect('members');

    }

    public function edit(Member $member)
    {
        
        $membercategory = MemberCategory::all();
        return view('pages.members.editmem', compact('member', 'membercategory'));
        // return response()->json($members);
    }
}
