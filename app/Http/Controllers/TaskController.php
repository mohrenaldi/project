<?php

namespace App\Http\Controllers;

use App\Task;
use Illuminate\Http\Request;

class TaskController extends Controller
{
    public function index(){
        $tasks = Task::all();
        return view('pages.task', compact('tasks'));
    }
    
    public function store()
    {
        request()->validate([
            'title' => 'required',
            'description' => 'required',
        ]);

        $tasks = new Task();
        $tasks->id = request('id');
        $tasks->title = request('title');
        $tasks->description = request('description');
        $tasks->save();

        return redirect('tasks');
    }

    public function delete(Task $task)
    {
        $task->delete();
        
        return back();
    
    }

    public function update(Task $task)
    {
        $task->update([
        'title' => request('title'),
        'description' => request('description'),
        ]);

        return redirect('tasks');
    }
    public function edit(Task $task)
    {
        return view('tasks.edit', compact('tasks'));
    }
}
    



    

