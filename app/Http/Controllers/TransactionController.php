<?php

namespace App\Http\Controllers;

use App\Transaction;
use App\Product;
use Carbon\Carbon;
use Illuminate\Http\Request;

class TransactionController extends Controller
{
    public function index(){
        $transactions = Transaction::all();
        $products = Product::all();
        return view('pages.transaction', compact('transactions','products'));
    }

    public function destroy($id)
    {
        $transactions=transaction::findOrFail($id);
        $transactions->delete();
        return back();
    
    }
    public function store()
    {

        request()->validate([
            'product_category_id' => 'required',
            'quantity' => 'required',
            
        ]);

        $rmdash = str_replace("-","",Carbon::now()->toDateTimeString());
        $rmcolon = str_replace(":","",$rmdash);
        $rmspace = str_replace(" ","",$rmcolon).substr(str_shuffle('0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ'),1,3);
        $trx_number = 'TRX'.$rmspace;

        $init = Product::where('id',request('product_category_id'))->get()->first()->amount;
        $total = request('quantity')*$init;
        //return response()->json($init);
        $transactions = new Transaction();
        $transactions->trx_number = $trx_number;
        $transactions->product_category_id = request('product_category_id');
        $transactions->quantity = request('quantity');
        $transactions->discount = 0;
        $transactions->total = $total;
        $transactions->save();

        return redirect('transactions');
    }

    public function update(Transaction $transaction)
    {
        $transaction->update([
        'product_category_id' => request('product_category_id'),
        'quantity' => request('quantity'),
        ]);

        return redirect('transactions');

    }
    public function edit(Transaction $transaction)
    {
        $products = Product::all();
        return view('pages.transactions.editt', compact('transaction', 'products'));
        // return response()->json($promos);
    }
}

