<?php

namespace App\Http\Controllers;

use App\Product;
use App\Category;
use DB;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function index(){
        $category = Category::all();
        $product = Product::all();
        return view('pages.product',compact('category', 'product'));
        // return response()->json($produt)
    }

    public function destroy($id)
    {
        $product = Product::findOrFail($id);
        $product->delete();
        return back();
    
    }

    public function store(Request $request)
    {

        // return response()->json($request);

        // $category = Category::all();
        $request->validate([
            'name' => 'required',
            'image'  =>  'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'product_category_id' => 'required',
            'desc' => 'required',
            'amount' => 'required',
        ]);
        // if ($request->has('active')) {
        //     $active = 1;
        //  } else {
        //      $active = 0;
        //  }

 
         $fileName = str_replace("=","",base64_encode($request->name.time())) . '.' . request()->image->getClientOriginalExtension();
 
         if(!$request->image->move(storage_path('app/public/promo'), $fileName)){
             return array('error' => 'Gagal upload foto');
         } else{

        $product = new Product();
        $product->name = $request->name;
        $product->image = "storage/promo/".$fileName;
        $product->product_category_id = $request->product_category_id;
        $product->desc = $request->desc;
        $product->amount = $request->amount;
        $product->save();

        }
        return redirect('products');
        // return response()->json($products);
    
    }
    
    public function update(Request $request, $id)
    {
        
        $productPict = Product::where("id","=",$id)->get()->first()->image;

        // return response()->json($productPict);

        if (!$request->image) {
            
            $request->validate([
                'name' => 'required',
                'product_category_id' => 'required',
                'desc' => 'required',
                'amount' => 'required'
            ]);
        } else {
            $request->validate([
                'name' => 'required',
                'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
                'product_category_id' => 'required',
                'desc' => 'required',
                'amount' => 'required'
            ]);

            $fileName = str_replace("=","",base64_encode($request->name.time())) . '.' . request()->image->getClientOriginalExtension();
        }
        
        if ($request->has('active')) {
            $active = 1;
        } else {
            $active = 0;
        }
        

        $product = Product::findOrFail($id);
        $product->name = $request->name;
        $product->product_category_id = $request->product_category_id;
        $product->desc = $request->desc;
        $product->amount = $request->amount;
        $product->image = $request->image;
        if($request->hasFile('image')){
            if (is_file($product->image)){
                try{
                    unlink($productPict);
                } catch(\Exception $e){

                }
            }
            $request->image->move(storage_path('app/public/produk'), $fileName);
            $product->image = "storage/produk/".$fileName;
        } else {
            $product->image = $productPict;
        }
        $product->save();

        return redirect('products');
    }
    
    public function edit(Product $product)
    {
        $category = Category::all();
        return view('pages.product.editp', compact('product', 'category'));
        
    }

}