<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public function index(){
        $category = Category::all();
        return view('pages.categoryproduct', compact('category'));
    }
    public function destroy($id)
    {
        $category=Category::findOrFail($id);
        $category->delete();
        return back();
    
    }
    public function store()
    {

        request()->validate([
            'name' => 'required',
            'desc' => 'required',
        ]);
        $category = new Category();
        $category->id = request('id');
        $category->name = request('name');
        $category->desc = request('desc');
        $category->save();

        return redirect('category');
    }
    public function update(Category $category)
    {
        $category->update([
        'name' => request('name'),
        'desc' => request('desc'),
        ]);

        return redirect('category');
    }
    public function edit(Category $category)
    {
        return view('pages.category.editc', compact('category'));
        // return response()->json($category);
    }
}