<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    protected $table='transactions';
    public function product(){
        return $this->belongsTo(Product::class, 'product_category_id');
    }
    protected $guarded=['id'];
}
