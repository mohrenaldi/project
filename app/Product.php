<?php

namespace App;


use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table='products';
    protected $guarded=['id'];

    public function product()
    {
        return $this->belongsTo(Category::class, 'product_category_id'); 
    }
}
