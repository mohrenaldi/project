@extends('app')

@section('content')

<div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>Input Task</h2>
                    <hr>
                    <ul class="header-dropdown m-r--5">
                        <li class="dropdown">
                            <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                <i class="material-icons">more_vert</i>
                            </a>
                            <ul class="dropdown-menu pull-right">
                                <li><a href="javascript:void(0);">Action</a></li>
                                <li><a href="javascript:void(0);">Another action</a></li>
                                <li><a href="javascript:void(0);">Something else here</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
                <div class="body">
                <div class="form-group">
                    <div class="row clearfix">
                        <div class="col-md-12">
                        <form action="" method="post" autocomplete="off" class="form-group">
                        @csrf
                                <div class="form-group">
                                  <label for="title">title</label>
                                  <div class="form-line">
                                  <input type="text" name="title" id="title" class="form-control" value="{{ $task->title }}">
                                  {!! $errors->first('title', '<span class="invalid-feedback">:message</span>') !!}
                                  </div>
                                  
                                </div>
                                <div class="form-group">
                                  <label for="description">description</label>
                                  <div class="form-line">
                                  <textarea type="text" name="description" id="description" class="form-control">{{ $task->description }}</textarea>
                                  {!! $errors->first('description', '<span class="invalid-feedback">:message</span>') !!}
                                  </div>
                                  </div>
                            <button type="submit" class="btn btn-primary">Save</button>
                        </form>
                        </div>
                    </div>
                </div>

                </div>
            </div>
        </div>
    </div>
    <!-- #END# Basic Table -->
    

@endsection