@extends('app')

@section('content')

<div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>Input Transaction</h2>
                    <hr>
                    <ul class="header-dropdown m-r--5">
                        <li class="dropdown">
                            <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                <i class="material-icons">more_vert</i>
                            </a>
                            <ul class="dropdown-menu pull-right">
                                <li><a href="javascript:void(0);">Action</a></li>
                                <li><a href="javascript:void(0);">Another action</a></li>
                                <li><a href="javascript:void(0);">Something else here</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
                <div class="body">
                <div class="form-group">
                    <div class="row clearfix">
                        <div class="col-md-12">
                        <form action="{{ route('transactions.update', $transaction->id) }}" method="post" autocomplete="off" class="form-group">
                        @csrf
                        @method('put')
                                  <div class="form-group">
                                    <label for="product_category_id">product_category_id</label>
                                    <div class="form-line">
                                        <select  class="form-control" name="product_category_id" id="product_category_id">
                                            <option disabled selected>Pilih Satu</option>
                                            @foreach($products as  $pro)
                                            <option {{ $transaction->product_category_id == $pro->id ? "selected" : "" }} value="{{ $pro->id }}">{{
                                                $pro->name }}</option>
                                            @endforeach
                                        </select> 
                                    </div>
                                  </div>
                                
                                <br><div class="form-group">
                                  <label for="quantity">quantity</label>
                                  <div class="form-line">
                                  <input type="quantity" name="quantity" id="quantity" class="form-control"value="{{ $transaction->quantity }}">
                                  {!! $errors->first('quantity', '<span class="invalid-feedback">:message</span>') !!}
                                  </div>
                                  </div>
                            <button type="submit" class="btn btn-primary">Save</button>
                        </form>
                        </div>
                        </div>
                </div>

                </div>
            </div>
            
        </div>
    </div>
    <!-- #END# Basic Table -->
    

@endsection                    