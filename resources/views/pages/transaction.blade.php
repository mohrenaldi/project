@extends('app')

@section('content')

<div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>Input Transaction</h2>
                    <hr>
                    <ul class="header-dropdown m-r--5">
                        <li class="dropdown">
                            <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                <i class="material-icons">more_vert</i>
                            </a>
                            <ul class="dropdown-menu pull-right">
                                <li><a href="javascript:void(0);">Action</a></li>
                                <li><a href="javascript:void(0);">Another action</a></li>
                                <li><a href="javascript:void(0);">Something else here</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
                <div class="body">
                <div class="form-group">
                    <div class="row clearfix">
                        <div class="col-md-12">
                        <form action="" method="post" autocomplete="off" class="form-group">
                        @csrf
                                  <div class="form-group">
                                    <label for="product_category_id">product_category_id</label>
                                    <div class="form-line">
                                        <select  class="form-control" name="product_category_id" id="product_category_id">
                                            <option disabled selected>Pilih Satu</option>
                                            @foreach($products as  $pro)
                                                <option value="{{ $pro->id }}"> {{ $pro->name }}</option>
                                            @endforeach
                                        </select> 
                                    </div>
                                  </div>
                                  
                                <br><div class="form-group">
                                  <label for="quantity">quantity</label>
                                  <div class="form-line">
                                  <input type="quantity" name="quantity" id="quantity" class="form-control">
                                  {!! $errors->first('quantity', '<span class="invalid-feedback">:message</span>') !!}
                                  </div>
                                </div>
                            <button type="submit" class="btn btn-primary">Save</button>
                        </form>
                        </div>
                        <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2> All Transactions </h2>
                            <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons">more_vert</i>
                                    </a>
                                    <ul class="dropdown-menu pull-right">
                                        <li><a href="javascript:void(0);">Action</a></li>
                                        <li><a href="javascript:void(0);">Another action</a></li>
                                        <li><a href="javascript:void(0);">Something else here</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <div class="body table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>no</th>
                                        <th>trx_number</th>
                                        <th>product_category_id</th>
                                        <th>quantity</th>
                                        <th>discount</th>
                                        <th>total</th>
                                        
                                    </tr>
                                    
                                </thead>
                                <tbody>
                                @foreach ($transactions as $n => $tra)
                                <tr>
                                    <td>{{ $n+1 }}</td>
                                    <td>{{ $tra->trx_number }}</td>
                                    <td>{{ $tra->product['name'] }}</td>
                                    <td>{{ $tra->quantity }}</td>
                                    <td>{{ $tra->discount }}</td>
                                    <td>{{ $tra->total }}</td>
                                    <td><a href="{{ route('transactions.edit', $tra->id) }}"><button type="button" class="btn btn-success">Update</button></a></td>
                                    </form>
                                    <form action="{{ route('transactions.destroy', $tra->id) }}" method="post">
                                    @csrf
                                    @method('delete')
                                    <td><button type="submit" class="btn btn-danger">Delete</button></td>
                                    </form>
                                </tr>
                                @endforeach
                                </tbody>
                            </table>
                                <a href="transactions"><button type="submit" class="btn btn-primary">Create</button></a>   
                        </div>
                    </div>
                   
                </div>
            </div>
            
        </div>
    </div>

@endsection