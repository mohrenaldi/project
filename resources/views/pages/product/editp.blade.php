@extends('app')

@section('content')

<div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>Input Product</h2>
                    <hr>
                    <ul class="header-dropdown m-r--5">
                        <li class="dropdown">
                            <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                <i class="material-icons">more_vert</i>
                            </a>
                            <ul class="dropdown-menu pull-right">
                                <li><a href="javascript:void(0);">Action</a></li>
                                <li><a href="javascript:void(0);">Another action</a></li>
                                <li><a href="javascript:void(0);">Something else here</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
                <div class="body">
                <div class="form-group">
                    <div class="row clearfix">
                        <div class="col-md-12">
                        <form action="{{ route('products.update', $product->id) }}" method="post" autocomplete="off" class="form-group" enctype="multipart/form-data">
                        @csrf
                        @method('put')
                                <div class="form-group">
                                  <label for="title">name</label>
                                  <div class="form-line">
                                  <input type="text" name="name" id="name" class="form-control" value="{{ $product->name }}">
                                  {!! $errors->first('name', '<span class="invalid-feedback">:message</span>') !!}
                                  </div>
                                  
                                </div>
                                <div class="form-group">
                                <div class="fallback">
                                  <label>Upload foto</label>
                                  <input type="file" name="image" id="image" class="form-control">
                                </div>

                                  </div>
                                  <div class="form-group">
                                    <label for="product_category_id">category</label>
                                    <div class="form-line">
                                        <select  class="form-control" name="product_category_id" id="product_category_id">
                                            <option disabled selected>Pilih Satu</option>
                                            @foreach($category as  $cat)
                                                <option {{ $product->product_category_id == $cat->id ? "selected" : "" }} value="{{ $cat->id }}">{{
                                                $cat->name }}</option>
                                            @endforeach
                                        </select> 
                                    </div>
                                  </div>
                                  <br><div class="form-group">
                                  <label for="desc">desc</label>
                                  <div class="form-line">
                                  <textarea type="text" name="desc" id="desc" class="form-control">{{ $product->desc }}</textarea>
                                  {!! $errors->first('desc', '<span class="invalid-feedback">:message</span>') !!}
                                  </div>

                                  </div>
                                <div class="form-group">
                                  <label for="image">amount</label>
                                  <div class="form-line">
                                  <input type="text" name="amount" id="amount" class="form-control" value="{{ $product->amount }}">
                                  {!! $errors->first('amount', '<span class="invalid-feedback">:message</span>') !!}
                                  </div>
                                  </div>
                            <button type="submit" class="btn btn-primary">Save</button>
                        </form>
                        </div>
                    </div>
                </div>

                </div>
            </div>
        </div>
    </div>
    <!-- #END# Basic Table -->
    

@endsection