@extends('app')

@section('content')

<div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>Input Task</h2>
                    <hr>
                    <ul class="header-dropdown m-r--5">
                        <li class="dropdown">
                            <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                <i class="material-icons">more_vert</i>
                            </a>
                            <ul class="dropdown-menu pull-right">
                                <li><a href="javascript:void(0);">Action</a></li>
                                <li><a href="javascript:void(0);">Another action</a></li>
                                <li><a href="javascript:void(0);">Something else here</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
                <div class="body">
                <div class="form-group">
                    <div class="row clearfix">
                        <div class="col-md-12">
                        <form action="" method="post" autocomplete="off" class="form-group">
                        @csrf
                                <div class="form-group">
                                  <label for="title">title</label>
                                  <div class="form-line">
                                  <input type="text" name="title" id="title" class="form-control">
                                  {!! $errors->first('title', '<span class="invalid-feedback">:message</span>') !!}
                                  </div>
                                  
                                </div>
                                <div class="form-group">
                                  <label for="description">description</label>
                                  <div class="form-line">
                                  <textarea type="text" name="description" id="description" class="form-control"></textarea>
                                  {!! $errors->first('description', '<span class="invalid-feedback">:message</span>') !!}
                                  </div>
                                  </div>
                            <button type="submit" class="btn btn-primary">Save</button>
                        </form>
                        </div>
                    </div>
                </div>
                <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2> All Task </h2>
                            <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons">more_vert</i>
                                    </a>
                                    <ul class="dropdown-menu pull-right">
                                        <li><a href="javascript:void(0);">Action</a></li>
                                        <li><a href="javascript:void(0);">Another action</a></li>
                                        <li><a href="javascript:void(0);">Something else here</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <div class="body table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>id</th>
                                        <th>title</th>
                                        <th>description</th> 
                                    </tr>
                                    
                                </thead>
                                <tbody>
                                    @foreach ($tasks as $n => $task)
                                        <tr>
                                            <td>{{ $n+1 }}</td>
                                            <td>{{ $task->title }}</td>
                                            <td>{{ $task->description }}</td>
                                            <form action="/tasks/{{ $task->id }}/edit" method="post" autocomplete="off">
                                            @csrf
                                            <td><a href="tasks"><button type="submit" class="btn btn-success">Update</button></a></td>
                                            </form>
                                            <form action="/tasks/{{ $task->id }}/delete" method="post">
                                            @csrf
                                            <td><button type="submit" class="btn btn-danger">Delete</button></td>
                                            </form>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                                            <a href="tasks"><button type="submit" class="btn btn-primary">Create</button></a>     
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- #END# Basic Table -->
    

@endsection