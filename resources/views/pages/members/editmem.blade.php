@extends('app')

@section('content')

<div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>Input Member</h2>
                    <hr>
                    <ul class="header-dropdown m-r--5">
                        <li class="dropdown">
                            <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                <i class="material-icons">more_vert</i>
                            </a>
                            <ul class="dropdown-menu pull-right">
                                <li><a href="javascript:void(0);">Action</a></li>
                                <li><a href="javascript:void(0);">Another action</a></li>
                                <li><a href="javascript:void(0);">Something else here</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
                <div class="body">
                <div class="form-group">
                    <div class="row clearfix">
                        <div class="col-md-12">
                        <form action="{{ route('members.update', $member->id) }}" method="post" autocomplete="off" class="form-group">
                        @csrf
                        @method('put')
                        <div class="form-group">
                                <div class="form-line">
                                    <select  class="form-control show-tick" name="member_category_id" id="member_category_id">
                                <option disabled selected>Categories</option>
                                @foreach ($membercategory as $memcat)
                                <option {{ $member->member_category_id == $memcat->id ? "selected" : "" }} value="{{ $memcat->id }}">{{
                                                $memcat->name }}</option>    
                                @endforeach
                            </select>
                        </div>
                        </div>
                        <br><div class="form-group">
                                  <label for="Full name">Full name</label>
                                  <div class="form-line">
                                  <input type="text" name="fullname" id="fullname" class="form-control" value="{{ $member->fullname }}">
                                  {!! $errors->first('Full name', '<span class="invalid-feedback">:message</span>') !!}
                                  </div>
                                </div>
                        <div class="input-group date" id="bs_datepicker_component_container">
                            <label for="dob">Tanggal Lahir</label>
                                <div class="form-line">
                                    <input type="date" name="dob" id="dob" class="form-control" value="{{ $member->dob }}" placeholder="Please choose a date...">
                                </div>
                            </div>
                            <div class="form-group">
                                  <label for="address">Address</label>
                                  <div class="form-line">
                                  <input type="text" name="address" id="address" class="form-control" value="{{ $member->address }}">
                                  {!! $errors->first('address', '<span class="invalid-feedback">:message</span>') !!}
                                  </div>
                                </div>
                        <div class="form-group">
                        <label for="gender">Gender</label>
                            <div class="demo-radio-button"><br>
                                <input name="gender" type="radio" id="laki-laki" value="{{ $member->gender }}" class="with-gap" checked />
                                <label for="laki-laki">Laki-laki</label>
                                <input name="gender" type="radio" id="perempuan" value="{{ $member->gender }}" class="with-gap" />
                                <label for="perempuan">Perempuan</label>
                            </div>
                        </div>
                        <div class="form-group">
                                  <label for="barcode">Barcode</label>
                                  <div class="form-line">
                                  <input type="text" name="barcode" id="barcode" class="form-control" value="{{ $member->barcode }}">
                                  {!! $errors->first('barcode', '<span class="invalid-feedback">:message</span>') !!}
                                </div>
                            </div>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
    </div>
    </div>
    </div>
    <!-- #END# Basic Table -->
    

@endsection