@extends('app')

@section('content')

<div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>Input Product</h2>
                    <hr>
                    <ul class="header-dropdown m-r--5">
                        <li class="dropdown">
                            <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                <i class="material-icons">more_vert</i>
                            </a>
                            <ul class="dropdown-menu pull-right">
                                <li><a href="javascript:void(0);">Action</a></li>
                                <li><a href="javascript:void(0);">Another action</a></li>
                                <li><a href="javascript:void(0);">Something else here</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
                <div class="body">
                <div class="form-group">
                    <div class="row clearfix">
                        <div class="col-md-12">
                        <form action="" method="post" autocomplete="off" class="form-group" enctype="multipart/form-data">
                        @csrf
                                <div class="form-group">
                                  <label for="name">name</label>
                                  <div class="form-line">
                                  <input type="text" name="name" id="name" class="form-control">
                                  {!! $errors->first('name', '<span class="invalid-feedback">:message</span>') !!}
                                  </div>
                                  
                                </div>
                                <div class="form-group">
                                <div class="fallback">
                                  <label>Upload foto</label>
                                  <input type="file" name="image" id="image" class="form-control">
                                
                                  </div>

                                </div>
                                <div class="form-group">
                                    <label for="product_category_id">category</label>
                                    <div class="form-line">
                                        <select  class="form-control" name="product_category_id" id="product_category_id">
                                            <option disabled selected>Pilih Satu</option>
                                            @foreach($category as  $cat)
                                                <option value="{{ $cat->id }}"> {{ $cat->name }}</option>
                                            @endforeach
                                        </select> 
                                    </div>
                                  </div>
                                  <br><div class="form-group">
                                  <label for="desc">desc</label>
                                  <div class="form-line">
                                  <textarea type="text" name="desc" id="desc" class="form-control"></textarea>
                                  {!! $errors->first('desc', '<span class="invalid-feedback">:message</span>') !!}
                                  </div>

                                </div>
                                <div class="form-group">
                                  <label for="ampount">amount</label>
                                  <div class="form-line">
                                  <input type="text" name="amount" id="amount" class="form-control">
                                  {!! $errors->first('amount', '<span class="invalid-feedback">:message</span>') !!}
                                  </div>
                                  </div>
                            <button type="submit" class="btn btn-primary">Save</button>
                        </form>
                        </div>
                    </div>
                </div>
                <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2> All Product </h2>
                            <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons">more_vert</i>
                                    </a>
                                    <ul class="dropdown-menu pull-right">
                                        <li><a href="javascript:void(0);">Action</a></li>
                                        <li><a href="javascript:void(0);">Another action</a></li>
                                        <li><a href="javascript:void(0);">Something else here</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <div class="body table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>no</th>
                                        <th>name</th>
                                        <th>image</th>
                                        <th>product_category_id</th>
                                        <th>desc</th>
                                        <th>amount</th>
                                    </tr>
                                    
                                </thead>
                                <tbody>
                                @foreach ($product as $n => $pro)
                                <tr>
                                    <td>{{ $n+1 }}</td>
                                    <td>{{ $pro->name }}</td>
                                    <td><img src="{{ $pro->image }}" height="80px" alt="gambar"></td>
                                    <td>{{ $pro->product['name'] }}</td>
                                    <td>{{ $pro->desc }}</td>
                                    <td>{{ $pro->amount }}</td>
                                    <td><a href="{{ route('products.edit', $pro->id) }}"><button type="button" class="btn btn-success">Update</button></a></td>
                                   
                                    <form action="{{ route('products.destroy', $pro->id) }}" method="post">
                                    @csrf
                                    @method('delete') 
                                    <td><button type="submit" class="btn btn-danger">Delete</button></td>
                                    </form>
                                </tr>
                                @endforeach
                                </tbody>
                            </table>
                                <a href="products"><button type="submit" class="btn btn-primary">Create</button></a>   
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- #END# Basic Table -->
    

@endsection